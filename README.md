# dotfiles

[![Linux][bdg.gnu-linux]][href.gnu-linux]
[![Organic][bdg.organic]][href.digital-gardens]

> My [`dotfiles`][href.dotfiles], managed with [Chezmoi][href.chezmoi]

These files constitute the portable config for applications and environments I run on GNU/Linux
workstations and servers.

See also: [*alexcochran/dotfiles-lite*][href.dotfiles-lite], a pared-down version of this project
built for servers and lightweight systems.

## Config Management Tool: [chezmoi][href.chezmoi]

Similar to [Git][href.git], Chezmoi keeps a working tree of changes to dotfiles between the managed dotfiles
repository and the in-use config files in the `$HOME` directory. This keeps the working and saved states separate
until more files are added or the dotfiles are applied, overwriting the user's current configs.

## Bootstrap

Ensure chezmoi is installed. Clone this project to the default Chezmoi directory `$HOME/.local/share/chezmoi`
and apply in one step with [`chezmoi init`][href.chezmoi.init]:

```shell
chezmoi init https:gitlab.com/alexcochran/dotfiles --apply
```

## Operation

### Add/Remove Files to the Tree

Use `chezmoi add` on any file outside of the `$HOME/.local/share/chezmoi` to add it to the dotfiles project.
`chezmoi forget` will remove them from the tree.

### Overwrite Local Dotfiles w/ State from Origin

Pull changes from the repository and apply them locally with [`chezmoi update`][href.chezmoi.update]. Edit
files directly in the Chezmoi directory using any editor, just like any other project.

**If outside of the Chezmoi path, use `chezmoi edit` to change managed files.** This also requires using
`chezmoi re-add` to add that file's local state to the managed dotfiles.

## References

### Projects

1. [github.com/Matt-FTW/dotfiles](https://github.com/Matt-FTW/dotfiles)

<!-- Links -->
[href.dotfiles]: https://dotfiles.github.io/
[href.chezmoi]: https://www.chezmoi.io/
[href.gnu-linux]: https://www.linux.org/pages/download/
[href.digital-gardens]: https://maggieappleton.com/garden-history
[href.git]: https://git-scm.com
[href.chezmoi.init]: https://www.chezmoi.io/reference/commands/init/
[href.chezmoi.update]: https://www.chezmoi.io/reference/commands/update/
[href.dotfiles-lite]: https://gitlab.com/alexcochran/dotfiles-lite

<!-- Badges -->
[bdg.gnu-linux]: https://img.shields.io/badge/GNU\/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black
[bdg.organic]: https://img.shields.io/badge/Organic-0CAA41?style=for-the-badge
