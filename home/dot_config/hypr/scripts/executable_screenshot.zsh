#!/usr/bin/env zsh

# screenshot
# > Captures a screenshot using the cursor via grim and slurp, then sends to clipboard

grim -g "$(slurp)" - | wl-copy -t image/png
notify-send "Screenshot copied to clipboard"
