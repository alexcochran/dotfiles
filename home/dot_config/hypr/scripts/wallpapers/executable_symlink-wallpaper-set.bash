#!/usr/bin/env bash
#
# Create n symbolic links to random wallpapers in a given folder

# Check if number of wallpapers argument is provided
if [ $# -ne 1 ] || ! [[ $1 =~ ^[0-9]+$ ]]; then
	echo "Usage: $(basename "$0") <number_of_wallpapers>" >&2
	exit 1
fi

NUM_WALLPAPERS=$1
LINK_DIR="$HOME/.config/hypr/wallpapers"

# Ensure the destination directory exists
mkdir -p "$LINK_DIR"

# Create symlinks for the specified number of wallpapers
for ((i = 1; i <= NUM_WALLPAPERS; i++)); do
	# Get a random wallpaper using the utility script
	WALLPAPER=$("${DESKSCRIPTS}/wallpapers/get-random-wallpaper.bash")

	if [ "$WALLPAPER" != "" ]; then
		ln -sf "$WALLPAPER" "$LINK_DIR/monitor-$i"
	else
		echo "Error: Could not find wallpaper for monitor $i" >&2
		exit 1
	fi
done
