#!/usr/bin/env bash

# Default wallpaper directory if none specified
DEFAULT_DIR="$HOME/.config/assets/images/wallpapers"
WALLPAPER_DIR="${1:-$DEFAULT_DIR}"

# Verify directory exists
if [ ! -d "$WALLPAPER_DIR" ]; then
	echo "Error: Directory $WALLPAPER_DIR does not exist" >&2
	exit 1
fi

# Find one random wallpaper and output its path
find "$WALLPAPER_DIR" -type f \( -iname "*.jpg" -o -iname "*.png" \) | shuf -n 1
