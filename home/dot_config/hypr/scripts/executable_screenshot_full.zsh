#!/usr/bin/env zsh

grimblast copy output || exit
notify-send "Screenshot copied to clipboard"
