#!/usr/bin/env zsh

grimblast copy area || exit
notify-send "Screenshot copied to clipboard"
