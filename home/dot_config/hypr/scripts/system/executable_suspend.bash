#!/usr/bin/env bash
#
# Automatically suspend and hibernate after a specified amount of time
# using swayidle and systemd.

swayidle -w \
	timeout 1200 ' hyprlock ' \
	timeout 1600 ' hyprctl dispatch dpms off' \
	timeout 12000 'systemctl suspend' \
	resume ' hyprctl dispatch dpms on' \
	before-sleep 'hyprlock'
